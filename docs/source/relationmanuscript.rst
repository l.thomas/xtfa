Relations Between the Notions of the Manuscript and their Implementation in the Tool
====================================================================================

In this page, we detail where the notions presented in the manuscript are implemented in the tool.

The Flow State (6.2.2)
----------------------


The concept of flow state is implemented by the class :py:class:`xtfa.flows.FlowState`.

The Content of the Flow State
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* :py:attr:`xtfa.flows.FlowState.flow`: A reference to the corresponding flow

* :py:attr:`xtfa.flows.FlowState.atEdge`: A string used to identify the parents from which the flow states comes

* :py:attr:`xtfa.flows.FlowState.arrivalCurve`: The arrival curve of the flow at the corresponding observation point

* :py:attr:`xtfa.flows.FlowState.clock`: The clock used to observe the values inside the flow state

* :py:attr:`xtfa.flows.FlowState.flags` used to store optionnal flags. Useful for example for the ADAM computations.

* :py:attr:`xtfa.flows.FlowState.minDelayFrom` [resp., :py:attr:`xtfa.flows.FlowState.minDelayFrom`]: The :math:`d[u]` [resp., :math:`D[u]`] maps representing the lower [resp.,] latency bounds since :math:`u^*`, the output of the ancestor :math:`u`. In the :py:attr:`xtfa.flows.FlowState` class, there exit no explicit set of ancestors :math:`\mathcal{U}`. The ancestors are stored as keys of the :py:attr:`xtfa.flows.FlowState.minDelayFrom` and :py:attr:`xtfa.flows.FlowState.minDelayFrom` maps.

* :py:attr:`xtfa.flows.FlowState.rtoFrom`: The :math:`\lambda[u]` corresponding to an upper-bound on the Reordering late Time Offset (RTO) of the flow at the location of the flow state, with respect the order of its packets at :math:`u^*`, the output of :math:`u`.

Changing the Clock of a Flow State
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The clock used to observe a flow state can be change with :py:meth:`xtfa.flows.FlowState.changeClock`


The Computation Pipelines (6.2.3)
---------------------------------

The computation pipelines are implemented by the following files:

* The aggregate computation pipeline (ACP) is implemented in :py:mod:`xtfa.inputPipelines`

* The delay-bound computation pipeline is implemented in :py:mod:`xtfa.contentionPipelines`

* The flow-state computation pipeline is implemented in :py:mod:`xtfa.outputPipelines`

The differences between the names used in the manuscript (aggregate computation pipeline, ACP) and the names used in the tool (input pipeline) are because the latter have been selected before the former.
When we wrote the manuscript, we decided to change the name of the notions for clarification purposes.


