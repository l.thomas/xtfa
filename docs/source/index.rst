.. xTFA documentation master file, created by
   sphinx-quickstart on Thu Jul  7 17:14:06 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Experimental Modular TFA (xTFA)
================================


**Experimental modular TFA (xTFA)** is a tool for computing latency bounds in time-sensitive networks that contain cyclic dependencies, traffic regulators, packet replication and elimination functions and non-ideal clock.

The main ideas behind the tool are presented in my manuscript.
A discussion on their implementation in the tool is available here: :doc:`relationmanuscript`

This website contains the API reference: :doc:`xtfa`.

.. however, is here to answer to the following questions:

.. * Where can we find in the tool the ideas presented in the manuscript ?

.. * What differences exist between the algorithms in the manuscript and their implementation in the tool ?

.. * How do I run an example ?

.. * What is the pipeline auto-configuration feature ?


.. The website also contains the full API for the :doc:`xtfa`



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   relationmanuscript
   modules
   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
